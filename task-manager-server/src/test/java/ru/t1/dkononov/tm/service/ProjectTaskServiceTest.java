package ru.t1.dkononov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.api.services.dto.IProjectDTOService;
import ru.t1.dkononov.tm.api.services.dto.ITaskDTOService;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.marker.UnitCategory;
import ru.t1.dkononov.tm.migration.AbstractSchemaTest;
import ru.t1.dkononov.tm.repository.dto.ProjectDTORepository;
import ru.t1.dkononov.tm.repository.dto.TaskDTORepository;
import ru.t1.dkononov.tm.service.dto.ProjectDTOService;
import ru.t1.dkononov.tm.service.dto.ProjectTaskDTOService;
import ru.t1.dkononov.tm.service.dto.TaskDTOService;

import java.util.Objects;

import static ru.t1.dkononov.tm.constant.TestData.*;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest extends AbstractSchemaTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @Nullable
    private final ProjectDTORepository projectRepository = new ProjectDTORepository(connectionService.getEntityManager());

    @Nullable
    private final TaskDTORepository taskRepository = new TaskDTORepository(connectionService.getEntityManager());

    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private final ProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(projectService, taskService);

    @Before
    public void before() throws UserIdEmptyException, ProjectNotFoundException, LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");


        projectRepository.add(USER1.getId(), USER_PROJECT);
        projectRepository.add(USER1.getId(), USER_PROJECT2);

        taskRepository.add(USER1.getId(), USER_TASK);
        taskRepository.add(USER1.getId(), USER_TASK2);
    }

    @After
    public void after() throws UserIdEmptyException {
        projectRepository.clear(USER1.getId());
        taskRepository.clear(USER1.getId());

    }

    @Test
    public void bindTaskToProject() throws Exception {
        projectTaskService.bindTaskToProject(USER1.getId(), USER_PROJECT.getId(), USER_TASK.getId());
        Assert.assertTrue(Objects.equals(USER_TASK.getProjectId(), USER_PROJECT.getId()));
    }

    @Test
    public void removeProjectById() throws Exception {
        projectTaskService.removeProjectById(USER1.getId(), USER_PROJECT.getId());
        Assert.assertFalse(projectRepository.findById(USER_PROJECT.getId()) != null);
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER_PROJECT.getId(), USER_TASK.getId());
        Assert.assertNull(USER_TASK.getProjectId());
    }

}
