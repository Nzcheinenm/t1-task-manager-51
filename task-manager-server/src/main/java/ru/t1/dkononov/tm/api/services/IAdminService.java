package ru.t1.dkononov.tm.api.services;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public interface IAdminService {
    @SneakyThrows
    void dropScheme(@NotNull String token);

    @SneakyThrows
    void initScheme(@NotNull String token);
}
